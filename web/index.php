<?php
//Silex App - code below is from Silex Documenation

//autoloading
require_once __DIR__ . '/../vendor/autoload.php';

//app config
$app = new Silex\Application();
$app['debug'] = true;

// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

// Register the Twig templating engine; though not using it to its full extent
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path' => __DIR__ . '/views',
));

//routing
$app->get('/', function() use ($app) {
  return $app['twig']->render('index.php');
});

//execute
$app->run();

?>
