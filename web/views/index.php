<?php ?>

<!DOCTYPE html>
<html lang="en">

{% include 'layout/head.php' %}

<body>

    {% include 'layout/header.php' %}

    <div class="container-fluid" id="contentContainer">
        
        <div id="profileContainer"></div>
        
        <div class="row" id="search">
            <div class="col-sm-2" id="artistResults">

            </div>
            
            <div class="col-sm-7" id="albumResults">

            </div>
        
            <div class="col-sm-3" id="trackResults">

            </div>
        </div>
        
        {% include 'content/landing.php' %}

    </div><!-- end container -->

    {% include 'layout/footer.php' %}

</body>

</html>
