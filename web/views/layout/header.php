<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-music"></i> Music Search</a>
        </div>

        <form class="navbar-form navbar-left" role="search" id="searchForm">
          <div class="form-group">
              <input type="text" class="form-control" id="searchField" placeholder="Search" autofocus>
          </div>
          <button type="submit" class="btn btn-primary" id="searchButton">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </form>


        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li id="aboutApp"><a href="#about">About</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>