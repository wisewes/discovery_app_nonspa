<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data']['items'];

$count = count($items);
$singleCount = 0;
$albumCount = 0;
$compilationCount = 0;

for($i = 0; $i < $count; $i++):

    switch($items[$i]['album_type']):
        case "album":
            $albumCount += 1;
        break;
        case "single":
            $singleCount += 1;
        break;
        case "compilation":
            $compilationCount += 1;
        break;
    endswitch;
endfor;


?>
<div class="panel panel-default">
  <div class="panel-heading">
       <h4 class="panel-title">Albums</h4>
  </div>
  <div class="panel-body">
      
      <div class="col-sm-12"><?php

            //outputCarousel($items);

            outputList($items); ?>
            
      </div>

  </div>
            
</div>


<?php


function outputCarousel($items) {
?>

<div id="carousel-artist-profile-albums" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators"><?php
    
    $count = count($items);
    for($i = 0; $i < $count; $i++): 
      
        if($i == 0):
            $classState = "active";
        else:
            $classState = "";
        endif;
      ?>
    
        <li data-target="#carousel-artist-profile-albums" data-slide-to="<?php echo $i; ?>" class="<?php echo $classState; ?>"></li><?php
    endfor; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox"> <?php
    
    for($i = 0; $i < $count; $i++): 
      
        $classes = ($i == 0) ? "item active" : "item"; ?>
      
        <div class="<?php echo $classes; ?>">
          <img src="<?php echo $items[$i]['images'][0]['url']; ?>" alt="album art">
          <div class="carousel-caption">
            <?php //echo $items[$i]['name']; ?>
          </div>
        </div><?php
    
    endfor; ?>

  </div><!-- end inner -->

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-artist-profile-albums" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-artist-profile-albums" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    
</div> 
<hr>
<?php
}


function outputList($items) {
?>
    <ul class="media-list">

    <?php

    $count = count($items);
    for($i = 0; $i < $count; $i++): ?>
        
        <li class="media artistProfileAlbum" id="forArtist-album-<?php echo $items[$i]['id']; ?>">
            <div class="media-left">
                <img class="img-circle" src="<?php echo $items[$i]['images'][0]['url']; ?>" width="100" height="100">
            </div>
            <div class="media-body">
                <h3 class="media-heading"><?php echo $items[$i]['name']; ?></h3>
                <div></div>
                <p><span class="text-muted info">Click to load album tracks</span></p>
                <a class="btn btn-xs btn-warning pull-right" href="<?php echo $items[$i]['external_urls']['spotify']; ?>" target="_blank"><i class="fa fa-eye"></i> View on Spotify</a>
                <input type="hidden" class="albumId" value="<?php echo $items[$i]['id']; ?>">
                
            </div>
            
            <div id="trackListing-<?php echo $items[$i]['id']; ?>"></div>
        </li>

        <?php

    endfor; ?>

    </ul>
<?php
}

