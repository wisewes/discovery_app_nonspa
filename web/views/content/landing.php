<?php
    //landing page, strictly informational
?>

<div id="landing">

<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-4">
        <div class="jumbotron">
            <p>
                <span><i class="fa fa-star"></i></span> 
                Use the search bar above to search for artists, albums, or tracks.
            </p>
            <p>
                <span><i class="fa fa-star"></i></span> 
                Press enter or click the blue search icon to display search results.
            </p>
        </div>
    </div>
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
        <div class="well">
            <h3>Purpose</h3>
            <p>This app demonstrates web development principles in creating a SPA (Single Page Application) <em>without</em> taking advantage of any clever, modern frameworks such as Angular.js or Backbone.  All frontend work is done with jQuery with PHP scripts used for layout </p>
            <hr>
            <h3>Components</h3>
            <p>This app uses the following frameworks:</p>
            <ul>
                <li>Twitter Bootstrap 3 for a responsive grid and easy styling</li>
                <li>Font Awesome for scaleable icons</li>
                <li>jQuery 2.0</li>
                <li>PHP 5.6 with Silex Micro Framework</li>
                <li>Spotify REST API - for artists, albums, tracks</li>
                <li>Echo Nest REST API - for biographies, other meta data</li>
                <li>Deployed and running on Heroku</li>
            </ul>
        </div>
    </div>
    <div class="col-sm-1"></div>

</div>

<div class="row">
    <hr>
    <div class="">
        <div class="panel panel-info">
            <p class="text-center">Data provided by 
                <a href="https://developer.spotify.com/web-api/">Spotify REST API</a> and 
                <a href="http://the.echonest.com">Echo Nest API</a>
            </p>
            <p class="text-center"><em>Wes Wise, 2015</em></p>
        </div>
    </div>
</div>
    
</div>