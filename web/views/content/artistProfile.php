<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data'];

?>

<div class="row" id="artistProfile">
    
    <div class="col-sm-4">
        
        <div class="panel panel-default">
            <div class="panel-body">
                
                <div class="media">
                    <h2 class="media-heading"><?php echo $items['name']; ?></h2>
                    <div class="media-body">

                        <img class="img-responsive img-thumbnail media-object" src="<?php echo $items['images'][0]['url']; ?>" alt="artist image" width="100%">

                        <hr>
                        <dl class="dl-horizontal">
                          <dt>Popularity</dt>
                          <dd><?php echo $items['popularity']; ?> / 100</dd>
                          <dt>Number of Followers</dt>
                          <dd><?php echo $items['followers']['total']; ?></dd>
                            
                          <?php
                          //display genres if set
                          if(!empty($items['genres'])): ?>
                            <dt>Genres</dt>
                            <dd><?php echo ucwords(implode($items['genres'], ", ")); ?></dd><?php
                          endif; ?>
                            
                          <dt>View On Spotify</dt>
                          <dd>
                              <a class="btn btn-xs btn-success" href="<?php echo $items['external_urls']['spotify']; ?>" target="_blank"><i class="fa fa-eye"></i>  Listen on Spotify</a>
                          </dd>
                        </dl>
                    </div>
                </div>
            
            </div>
        </div>
        
        <div id="profileInfo">
            <p class="text-center"><i class="fa fa-5x fa-spinner fa-pulse"></i></p>
        </div>
        
        <hr>
        
        <div id="profileSimilar">
            <p class="text-center"><i class="fa fa-5x fa-spinner fa-pulse"></i></p>
        </div>
        
    </div>
    
    
    <div class="col-sm-6" id="profileAlbums">
        <p class="text-center"><i class="fa fa-5x fa-spinner fa-pulse"></i></p>
    </div>
    
    <div class="col-sm-2" id="profileTopTracks">
        <p class="text-center"><i class="fa fa-5x fa-spinner fa-pulse"></i></p>
    </div>
</div>

