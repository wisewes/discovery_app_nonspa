<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data'];

//var_dump($items['videos']);

$urls = $items['urls']; //no an array
//$videos = $items['videos'];
$bioItems = $items['bio']; 

/*** artist urls ***/

?>
<div class="col-sm-12">
    <div class="col-sm-4"><?php
        
        if(array_key_exists('official_url', $urls)): ?>
            <a class="infoIcons" href="<?php echo $urls['official_url']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View Artist's Official Site">
                <i class="fa fa-2x fa-bookmark" >  </i>
        </a><?php
        endif; ?>
        
    </div>
    
    <div class="col-sm-4"><?php

        if(array_key_exists('lastfm_url', $urls)): ?>
            <a class="infoIcons"  href="<?php echo $urls['lastfm_url']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View on Lastfm">
                <i class="fa fa-2x fa-lastfm">  </i>
        </a><?php
        endif; ?>
        
    </div>
    
    <div class="col-sm-4"><?php

        if(array_key_exists('wikipedia_url', $urls)): ?>
            <a class="infoIcons" href="<?php echo $urls['wikipedia_url']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Read on Wikipedia">
                <i class="fa fa-2x fa-info"></i>
        </a><?php
        endif; ?>

    </div>
</div>

<br>
<hr><?php

/*** artist videos ***/






/*** artist bio ***/


$fullBio = null;

foreach($bioItems as $bio):
    
    //skip all truncated bios
    if(array_key_exists('truncated', $bio)):
      continue;
    else:
      $fullBio = $bio;
      break;
    endif;
endforeach;


if(is_null($fullBio)): ?>
    <p>No Biography Found.</p><?php
else: ?>

    <button class="btn btn-xs btn-primary" type="button" data-toggle="collapse" data-target="#collapseArtistBio" aria-expanded="false" aria-controls="collapseArtistBio">
        <i class="fa fa-book"></i>  Read Bio
    </button>
    <hr>
    <div class="collapse" id="collapseArtistBio">
      <div class="well">
        <p><?php echo $fullBio['text']; ?></p>
        <p>
            <span class="label label-primary">Source</span>  
            <span class="text-muted"><?php echo $fullBio['site']; ?></span>
        </p>
      </div>
    </div>

    <?php
endif;



