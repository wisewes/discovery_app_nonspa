<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(isset($_POST['data']) && empty($_POST['data'])):
?>
    
    <div class="alert alert-danger">
        <p>No Albums Found.</p>
    </div> <?php
else:

    $items = $_POST['data'];
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matched Albums 
                <span class="badge pull-right"><?php echo count($items); ?></span>
            </h3>
        </div>
        <div class="panel-body">

            <div class="row"><?php

                loopAlbums($items); ?>

            </div>
        </div>
    </div><?php
endif;



function loopAlbums($items) {

    $length = count($items);
    for($i = 0; $i < $length; $i++):

        if(!isset($items[$i]['id']) || !isset($items[$i]['name'])):
            break;
        endif; ?>

        <div class="col-sm-3 album">

            <div class="media">
              <div class="media-body">
                    <a href="#" id="artist-<?php echo $items[$i]['artist']; ?>-<?php echo $items[$i]['artistName']; ?>" data-toggle="tooltip" data-placement="top" title="View Artist's Profile">
                        <?php

                    if(!empty($items[$i]['image'])): ?>
                        <img class="img-responsive media-object img-circle" src="<?php echo $items[$i]['image']; ?>" width="90%"><?php
                    else: ?>
                        <span class="thumbnail media-object"><i class="fa fa-5x fa-music"></i></span><?php
                    endif; ?>
    
                    </a>
              </div>
              <h5 class="media-heading text-center albumTitle"><?php echo $items[$i]['name']; ?></h5>
              <input type="hidden" class="albumId" value="<?php echo $items[$i]['id']; ?>">
              <input type="hidden" class="albumURI" value="<?php echo $items[$i]['uri']; ?>">
            </div>
        </div>

    <?php

    endfor; ?>
<?php
}