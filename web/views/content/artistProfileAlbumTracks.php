<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data'];


?>


<h4>Tracks</h4>
<table class="table table-condensed">
    <thead>
        <th>#</th>
        <th>Title</th>
        <th><i class="fa fa-clock-o"></i></th>
        <th><i class="fa fa-eye"></i></th>
        <th><i class="fa fa-headphones"></i></th>
    </thead>
    <tbody><?php

        $count = count($items); 
        for($i = 0; $i < $count; $i++): ?>
            
            <tr>
                <input type="hidden" class="trackId" value="<?php echo $items[$i]['id']; ?>">
                <td><?php echo $items[$i]['track_number']; ?></td>
                <td><?php echo $items[$i]['name']; ?></td>
                <td><?php echo date("i:s", $items[$i]['duration_ms'] / 1000); ?></td>
                <td>
                    <a class="btn btn-xs btn-info" href="<?php echo $items[$i]['external_urls']['spotify']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Listen to song on Spotify">
                        <i class="fa fa-eye"></i>
                    </a>
                </td>
                <td>
                    <span class="trackPreviewWrapper">
                        <button class="btn btn-xs startTrackSample" data-toggle="tooltip" data-placement="top" title="Hear 30 second sample">
                            <i class="fa fa-play"></i>
                        </button>
                        <audio src="<?php echo $items[$i]['preview_url']; ?>">
                            N/A
                        </audio>
                    </span>
                </td>
            </tr><?php

        endfor; ?>


    </tbody>
</table>

