<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data'];


?>


<div class="">
    <h3>Similar Musicians</h3>
    
    <?php

    $count = count($items);
    for($i = 0; $i < $count; $i++): ?>
        <div class="media col-sm-3" id="artist-<?php echo $items[$i]['id']; ?>-<?php echo $items[$i]['name']; ?>">
        <div class="media-body">
            <img class="media-object img-reponsive img-rounded" src="<?php echo $items[$i]['images'][0]['url']; ?>" width="75px" height="75px">
            <h5 class="media-heading"><?php echo $items[$i]['name']; ?></h5>
        </div>
        </div>

    <?php
    endfor; ?>
</div>