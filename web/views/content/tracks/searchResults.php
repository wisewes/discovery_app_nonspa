<?php

if(!isset($_POST['data'])):
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(isset($_POST['data']) && empty($_POST['data'])):
?>
    
    <div class="alert alert-danger">
        <p>No Artists Found.</p>
    </div> <?php
else:

    $items = $_POST['data'];
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matched Tracks 
                <span class="badge pull-right"><?php echo count($items); ?></span>
            </h3>
        </div>
        <div class="panel-body"><?php
            
            loopTracks($items); ?>
            
        </div>
    </div>

<?php
endif;



function loopTracks($items) {
?>
    <ul class="list-group"><?php

    $length = count($items);
    for($i = 0; $i < $length; $i++): 
    
        ?>
        
        <li class="list-group-item"><?php

            //get first image
            if(isset($items[$i]['albumImage'])):
               $imageUrl = $items[$i]['albumImage'];
            else:
               $imageUrl = null;
            endif;  ?>
            
                <div class="media">
                  <div class="media-body">
                    <h4 class="media-heading"><?php echo $items[$i]['name']; ?></h4><?php
    
                    if(is_null($imageUrl)): ?>
                      <span class="thumbnail"><i class="fa fa-5x fa-music"></i></span><?php
                    else: ?>
                        <img class="media-object img-responsive" src="<?php echo $imageUrl; ?>" alt="artist image" width="115px" height="115px"><?php
                    endif; ?>
                      
                    <p>On <em><?php echo $items[$i]['albumName']; ?></em></p>   
                      
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <span class="label label-default">How to Listen</span>
                        </div>
                    
                        <div class="col-sm-3"><?php
                            
                            if(isset($items[$i]['preview'])): ?>
    
                                <button class="btn btn-xs btn-default startTrackSample" data-toggle="tooltip" data-placement="top" title="Hear 30 second sample">
                                <i class="fa fa-2x fa-play"></i>
                                </button>
                                <audio src="<?php echo $items[$i]['preview']; ?>">
                                    N/A
                                </audio><?php
                        
                            endif; ?> 
                        </div>
                        
                        <div class="col-sm-3">
                            <a class="btn btn-xs btn-default" href="<?php echo $items[$i]['spotify']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Listen to song on Spotify">
                                <i class="fa fa-2x fa-eye"></i>
                            </a>  
                        </div>
                    </div>
                      
                    <input type="hidden" class="trackId" value="<?php echo $items[$i]['id']; ?>">
                    <input type="hidden" class="trackURI" value="<?php echo $items[$i]['uri']; ?>">
                    <input type="hidden" class="trackHref" value="<?php echo $items[$i]['href']; ?>">
                  </div>
                </div>
            

                
                <?php //print_r($items[$i]); ?>
        </li>
    <?php
    endfor;

    ?>
    </ul><?php
}


