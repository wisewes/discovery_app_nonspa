<?php

if(!isset($_POST['data'])): 
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(empty($_POST['data'])):
   
   echo "no data";
   exit;
endif;

$items = $_POST['data'];

?>

<div class="panel panel-default">
  <div class="panel-heading">
       <h4 class="panel-title">Top 10 Tracks</h4>
  </div>
  <div class="panel-body">

        <div class="list-group"> <?php

            $count = count($items);
            for($i = 0; $i < $count; $i++): ?>

              <div class="list-group-item" id="album-<?php echo $items[$i]['id']; ?>">
                <h4 class="list-group-item-heading"><?php echo $items[$i]['name']; ?></h4>
                <div class="list-group-item-text">
                    <h5>On <em><?php echo $items[$i]['album']['name']; ?></em></h5>
                    <img class="img-responsive img-thumbnail" src="<?php echo $items[$i]['album']['images'][0]['url']; ?>" height="115px" width="115px">     
 
                    <hr>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            
                            <button class="btn btn-xs startTrackSample" data-toggle="tooltip" data-placement="top" title="Hear 30 second sample">
                            <i class="fa fa-play"></i>
                            </button>
                            <audio src="<?php echo $items[$i]['preview_url']; ?>">
                            N/A
                            </audio>
                              
                        </div>
                        <div class="col-sm-6">
                            
                            <a class="btn btn-xs btn-default" href="<?php echo $items[$i]['external_urls']['spotify']; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Listen to song on Spotify">
                                <i class="fa fa-eye"></i>
                            </a> 
                            
                        </div>
                    
                    </div>
                    <br>
                    
                </div>
              </div>
            <?php

            endfor; ?>
        </div>
      
  </div>
</div>