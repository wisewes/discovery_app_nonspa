<?php

if(!isset($_POST['data'])):
    
    throw new Exception('$_POST["data"] is empty!');
    exit;
endif;

if(isset($_POST['data']) && empty($_POST['data'])):
?>
    
    <div class="alert alert-danger">
        <p>No Artists Found.</p>
    </div> <?php
else:

    $items = $_POST['data'];
    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matched Artists 
                <span class="badge pull-right"><?php echo count($items); ?></span>
            </h3>
        </div>
        <div class="panel-body"><?php
            
            loopArtists($items); ?>
            
        </div>
    </div>

    <?php
    
endif;
exit;


function loopArtists($items) {
?>
<div class="list-group"><?php

    $length = count($items);
    for($i = 0; $i < $length; $i++): 

        $name = $items[$i]['name'];

        if(isset($items[$i]['images'])):
            $imageUrl = $items[$i]['images'][0]['url'];
        else:
           $imageUrl = null;
        endif;  ?>

        <a href="#" id="artist-<?php echo $items[$i]['id']; ?>-<?php echo $name; ?>" class="list-group-item">
            <div clas="list-group-item-text">
                
            <div class="media" data-toggle="tooltip" data-placement="top" title="Click to view <?php echo $name; ?>'s profile">
                <div class="media-body">
                <h4 class="media-heading text-center"><?php echo $name; ?></h4><?php
    
                    if(is_null($imageUrl)): ?>
                      <span><i class="fa fa-8x fa-music"></i></span><?php
                    else: ?>
                      <img class="media-object img-responsive img-rounded" src="<?php echo $imageUrl; ?>" alt="artist image" width="100%"><?php
                    endif; ?>
                
                </div>
            </div>

            </div>
            <input type="hidden" class="artistId" value="<?php echo $items[$i]['id']; ?>">
            <input type="hidden" class="artistURI" value="<?php echo $items[$i]['uri']; ?>">
        </a><?php

    endfor; ?>

</div>
<?php
}



