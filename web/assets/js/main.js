/*jslint browser: true */
/*global $, jQuery, alert, console */
'use strict';

$(document).ready(function() {
    
    
    var CONFIG = {
        SCRIPTS_PATH: 'views/content',
        ECHONEST_API_KEY: 'ASGIGF8WY5HB0EMNG'
    };
    
    
    //enable twitter bootstrap tooltips
    $(function () {
        $('body').tooltip({
            selector: '[data-toggle=tooltip]'
        });
    });
    
    $(function() {
        $('#searchField').val('');
    });
    

    /**
     * Module responsible for handling an artist's profile
     */
    var ArtistProfile = (function() {
        
        var _selectors = {
            content: '#contentContainer'
        };
        
        //object that holds spotify and name 
        var _artistIds = null;
        
        /**
         * helper function that removes array of market values; even specifying market or country
         * will still return array of all available markets 
         * @param {Object} json-object 
         */
        var _pruneAvailableMarkets = function(data) {
            
            if('tracks' in data) {
                
                for(var i = 0; i < data.tracks.length; i++) {
                    delete data.tracks[i].available_markets; 
                }
                
            } else if('artists' in data) {
                
                for(var i = 0; i < data.artists.length; i++) {
                    delete data.artists[i].available_markets;
                }
            } else {
                        
                for(var i = 0; i < data.items.length; i++) {
                    delete data.items[i].available_markets;
                }
            }
        };
        
        
        /**
         * starts process of loading an artist's profile
         * @param   {String} artistId spotify artist object id
         * @param   {String} name     string of the artist
         * @returns {Object} artist profile data in json - deferred object
         */
        var loadArtistProfile = function(artistId, name) {
            
            _artistIds = {id: artistId, name: name};
            
            //get artist info /artists/{id}
            var getArtistById  = function(artistId) {
            
                return $.ajax({
                    type: 'GET',
                    url: 'https://api.spotify.com/v1/artists/' + artistId,
                    success: function(artistData) {
                        console.log('called getArtistsById()');
                    }
                });
            };
            
            
            /**
             * get albums for artist /artists/{id}/albums
             * @param   {String} artistId spotify object id
             * @returns {Object} artist albums in json - deferred object
             */
            var getArtistAlbums = function(artistId) {
            
                return $.ajax({
                    type: 'GET',
                    url: 'https://api.spotify.com/v1/artists/' + artistId + '/albums',
                    data: { limit: 50, market: 'US' }, //max allowed is 50
                    success: function(albumData) {
                        console.log('called getArtistAlbums()');
                        
                        //prune results to filter out available markets
                        _pruneAvailableMarkets(albumData);
                    }
                });
            };
        
            
            /**
             * get top tracks for artist /artists/{id}/top-tracks?country=US
             * endpoint only supports max of 10 results
             * @param   {String} artistId spotify object id
             * @returns {Object} artists top tracks in json - deferred object
             */
            var getTopTracks = function(artistId) {
                
                return $.ajax({
                    type: 'GET',
                    url: 'https://api.spotify.com/v1/artists/' + artistId + '/top-tracks',
                    data: { country: 'US' },
                    success: function(topTracks) {
                        console.log('called getTopTracks()');
                        
                        //prune results to filter out available markets
                        _pruneAvailableMarkets(topTracks);
                    }
                });
            };
            
            
            /**
             * get related artists /artist/{id}/related-artists
             * @param   {String} artistId spotify object id 
             * @returns {Object} similar artists in json - deferred object
             */
            var getRelatedArtists = function(artistId) {
            
                return $.ajax({
                    type: 'GET',
                    url: 'https://api.spotify.com/v1/artists/' + artistId + '/related-artists',
                    success: function(relatedArtists) {
                        console.log('called getRelatedArtists()');
                        
                        //prune results to filter out available markets
                        _pruneAvailableMarkets(relatedArtists);
                    }
                });
            };
            

            /**
             * get biographical inforamtion for an artist - uses Echonest API
             * @returns {Object} array of biographies in json - deferred object
             */
            var getArtistBiography = function() {

                return $.ajax({
                    type: 'GET',
                    url: 'https://developer.echonest.com/api/v4/artist/search',
                    data: { 
                        api_key: CONFIG.ECHONEST_API_KEY,
                        format: 'json',
                        name: _artistIds.name,
                        bucket: 'biographies',
                    },
                    success: function(bio) {
                        console.log('called getArtistBiography()');
                    }
                });
            };
            
 
            /**
             * get social urls from Echonest API
             * @returns {Object} json object of urls - deferred object
             */
            var getArtistUrls = function() {

                return $.ajax({
                    type: 'GET',
                    url: 'https://developer.echonest.com/api/v4/artist/search',
                    data: { 
                        api_key: CONFIG.ECHONEST_API_KEY,
                        format: 'json',
                        name: _artistIds.name,
                        bucket: 'urls',
                    },
                    success: function(bio) {
                        console.log('called getArtistUrls()');
                    }
                });
            };
            

            //*** chain above functions together 
            $.when(
                getArtistById(artistId), 
                getArtistAlbums(artistId), 
                getTopTracks(artistId), 
                getRelatedArtists(artistId),
                getArtistBiography(), 
                getArtistUrls())
            .done(function(artist, albums, topTracks, relatedArtists, biography, urls) { //success block
                
                var resultSet = {
                    artist: artist[0], 
                    albums: albums[0], 
                    topTracks: topTracks[0].tracks,
                    relatedArtists: relatedArtists[0].artists,
                    bio: biography[0].response.artists[0].biographies,
                    urls: urls[0].response.artists[0].urls
                };
                
                console.log(resultSet);

                //load all found data
                $.ajax({
                    type: 'POST',
                    url: CONFIG.SCRIPTS_PATH + '/artistProfile.php', 
                    data: { data: resultSet.artist },
                    success: function(data) {
                        
                        //ui change
                        $('#profileContainer').empty().append(data);
                        $('#search').hide();
                    },
                    complete: function() {
                        console.log('loaded artist profile complete');
                        
                        $.post(CONFIG.SCRIPTS_PATH + '/artistProfileSimilar.php', { data: resultSet.relatedArtists }, 
                            function(data) {
                                $('body').find('#profileSimilar').empty().append(data);
                        });
                        
                        $.post(CONFIG.SCRIPTS_PATH + '/artistProfileTopTracks.php', { data: resultSet.topTracks },
                            function(data) {
                                $('body').find('#profileTopTracks').empty().append(data);
                        });
                        
                        $.post(CONFIG.SCRIPTS_PATH + '/artistProfileAlbums.php', { data: resultSet.albums }, 
                            function(data) {
                                $('body').find('#profileAlbums').empty().append(data);
                        });
                        
                        $.post(CONFIG.SCRIPTS_PATH + '/artistProfileInfo.php', 
                            { data: { bio: resultSet.bio, urls: resultSet.urls, videos: resultSet.videos }},
                            function(data) {
                                $('body').find('#profileInfo').empty().append(data);
                        });
                    }
                });
            }); 
        };
        
        
        /**
         * load the tracks for a specified album
         * @param {String} albumdId spotify object id for an album
         */
        var loadTracksForAlbum = function(albumdId) {
        
            console.log('loadTracksForAlbum() id = ' + albumdId);
           
            $.ajax({
                type: 'GET',
                url: 'https://api.spotify.com/v1/albums/' + albumdId + '/tracks',
                beforeSend: function() {
                },
                success: function(data) {
                    
                    //filter out available markets subarray
                    for(var i = 0; i < data.items.length; i++) {
                        delete data.items[i].available_markets;
                    }
                    
                    $.post(CONFIG.SCRIPTS_PATH + '/artistProfileAlbumTracks.php', { data: data.items }, 
                        function(data) {

                        $('body').find('#trackListing-' + albumdId)
                            .empty().append(data).parents('li').addClass('loaded');
                    });
                }
            });
        };
        
        //expose
        return {
            loadArtistProfile: loadArtistProfile,
            loadTracksForAlbum: loadTracksForAlbum
        };
    })();
    
    
    
    /**
     * Module responsible for searching Spotify for data
     */
    var APIModule = (function() {
        
        var _selectors = {
            artists: '#artistResults',
            albums: '#albumResults',
            tracks: '#trackResults'
        };
        
        
        /**
         * format the json response for album data - filled with only data that is necessary
         * @param   {Object} dataSet json response object 
         * @returns {Object} resultSet object containing only necessary fields
         */
        var _structureAlbumsData = function(dataSet) {
        
            var resultSet = [];
            for(var i = 0; i < dataSet.albums.length; i++) {

                //filter out info that we need                  
                var obj = {
                    id:             dataSet.albums[i].id,
                    artist:         dataSet.albums[i].artists[0].id,
                    artistName:     dataSet.albums[i].artists[0].name,
                    name:           dataSet.albums[i].name,
                    uri:            dataSet.albums[i].uri,
                    href:           dataSet.albums[i].href,
                    image:          dataSet.albums[i].images[0].url,
                    release:        dataSet.albums[i].release_date,
                    genres:         dataSet.albums[i].genres,
                    tracksHref:     dataSet.albums[i].tracks.href,
                    tracksCount:    dataSet.albums[i].tracks.items.length
                };

                resultSet.push(obj);
            }
            
            return resultSet;
        };
        
        
        /**
         * format the json response for track data - filled with only data that is necessary
         * @param   {Object} dataSet json response object 
         * @returns {Object} resultSet object containing only necessary fields
         */
        var _structureTracksData = function(dataSet) {
        
            var resultSet = [];
            for(var i = 0; i < dataSet.tracks.length; i++) {
                
                //filter what we need
                var obj = {
                    id:              dataSet.tracks[i].id,
                    name:            dataSet.tracks[i].name,
                    uri:             dataSet.tracks[i].uri,
                    href:            dataSet.tracks[i].href,
                    spotify:         dataSet.tracks[i].external_urls.spotify,
                    duration:        dataSet.tracks[i].duration_ms,
                    preview:         dataSet.tracks[i].preview_url,
                    trackNumber:     dataSet.tracks[i].track_number,
                    artists:         dataSet.tracks[i].artists,
                    albumId:         dataSet.tracks[i].album.id,
                    albumName:       dataSet.tracks[i].album.name,
                    albumHref:       dataSet.tracks[i].album.href,
                    albumImage:      dataSet.tracks[i].album.images[0].url
                };
                
                resultSet.push(obj);
            }
            
            return resultSet;
        };
        
        
        /**
         * helper that loads in artist info for search results
         * @param {Object} artistData json object
         */
        var _displayArtists = function(artistData) {
            console.log('_displayArtists()');
            
            $.ajax({
                type: 'POST',
                url: CONFIG.SCRIPTS_PATH + '/artists/searchResults.php',
                data: { data: artistData }, 
                beforeSend: function() {
                },
                success: function(data) {
                    $(_selectors.artists).empty().append(data);
                }
            });
        };
        
        /**
         * helper that loads in album info for search results
         * @param {Object} albumData json object
         */
        var _displayAlbums = function(albumData) {
            console.log('_displayAlbums()');
            
            $.ajax({
                type: 'POST',
                url: CONFIG.SCRIPTS_PATH + '/albums/searchResults.php',
                data: { data: albumData.albums }, 
                beforeSend: function() {
                },
                success: function(data) {
                    $(_selectors.albums).empty().append(data);
                }
            });
        };
        
        /**
         * helper that loads in tracks info for search results
         * @param {Object} trackData json object    
         */
        var _displayTracks = function(trackData) {
            console.log('_displayTracks()');
            
            $.ajax({
                type: 'POST',
                url: CONFIG.SCRIPTS_PATH + '/tracks/searchResults.php',
                data: { data: trackData.tracks }, 
                beforeSend: function() {
                },
                success: function(data) {
                    $(_selectors.tracks).empty().append(data);
                }
            });
        };
        
        
        /**
         * @public
         * initiating funtion taht captures search field and sends search request
         * @param {String} query name of artist/musician
         */
        var search = function(query) {
        
            $.ajax({
                type: 'GET',
                url: 'https://api.spotify.com/v1/search',
                data: {type: 'artist,album,track', q: query },
                beforeSend: function() {
                    
                    var markup = '<p class="text-center"><i class="fa fa-5x fa-spinner fa-pulse"></i></p>';
                    $('#artistResults, #albumResults, #trackResults').empty().append(markup);
                },
                success: function(data) {
                    
                    var noResults = '<div class="alert alert-danger"><p>No Results</p></div>';

                    if('artists' in data) {
                        
                        if(data.artists.total === 0) {
                            $('#artistResults').empty().append(noResults);
                        } else {
                            
                            _displayArtists(data.artists.items);
                        }
                    }

                    if('albums' in data) {
                        
                        if(data.albums.total === 0) {
                            $('#albumResults').empty().append(noResults);
                        } else {
                        
                            //collect object ids for albums
                            var albumIds = [];

                            for(var i = 0; i < data.albums.items.length; i++) {
                                albumIds.push(data.albums.items[i].id);
                            }

                            searchMultipleAlbums(albumIds);
                        }
                    }

                    if('tracks' in data) {

                        if(data.tracks.total === 0) {
                            $('#trackResults').empty().append(noResults);
                        } else {
                            //collect object ids for tracks
                            var trackIds = [];

                            for(var i = 0; i < data.tracks.items.length; i++) {
                                trackIds.push(data.tracks.items[i].id);
                            }

                            searchMutipleTracks(trackIds);
                        }
                    }
                },
                error: function(err) {

                }, complete: function() {
                }
            });
        };
        
        
        /**
         * get multiple albums by their ids; comma delimited string
         * @param {Array} ids spotify album object ids
         */
        var searchMultipleAlbums = function(ids) {
            
            var idsAsString = ids.join(',');
            
            $.ajax({
                type: 'GET',
                url: 'https://api.spotify.com/v1/albums',
                data: { ids: idsAsString },
                beforeSend: function() {
                
                },
                success: function(data) {
  
                    //filter out only the data that is needed
                    var resultSet = _structureAlbumsData(data);
                    
                    //post to PHP to structure HTML
                    $.post(CONFIG.SCRIPTS_PATH + '/albums/searchResults.php', { data: resultSet }, function(data) {
                        $(_selectors.albums).empty().append(data);
                    }); 
                },
                complete: function(data) {
                    console.log('searchMultipleAlbums()');
                }
            });
        };
        

        /**
         * get multiple tracks by their ids; comma delimited string
         * @param {Array} ids spotify track object ids
         */
        var searchMutipleTracks = function(ids) {
        
            var idsAsString = ids.join(',');
            
            $.ajax({
                type: 'GET',
                url: 'https://api.spotify.com/v1/tracks',
                data: { ids: idsAsString },
                beforeSend: function() {
                
                },
                success: function(data) {
                    
                    var resultSet = _structureTracksData(data);
                    
                    $.post(CONFIG.SCRIPTS_PATH + '/tracks/searchResults.php', { data: resultSet }, function(data) {
                        $(_selectors.tracks).empty().append(data);
                    }); 
                },
                complete: function(data) {
                    console.log('searchMultipleTracks()');
                }
            });
        };
        
        
        //expose
        return {
            search: search,
            searchMultipleAlbums: searchMultipleAlbums,
            searchMutipleTracks: searchMutipleTracks
        };
        
    })(); // APIModule
    
    
    
    
    /***************************************************************
        Click Events
    ****************************************************************/
    
    
    //respond to search button - click event
    $('body').on('click', '#searchButton', function(e) {
        e.preventDefault();

        var term = $('#searchField').val();

        if(term.length === 0) {

            $(this).prev().addClass('has-error');
            $(this).prop('autofocus', true);
            alert('Enter a term to search. Terms can include artist name, album title, and song title.');
            return;
        }

        //ui change
        $('#search').show();
        $('#artistProfile').remove();
        $('#landing').hide();
        
        $(this).prev().removeClass('has-error');
        APIModule.search(term);
    });
    
    
    //respond to navbar click - click event
    $('body').on('click', 'a.navbar-brand', function(e) {
        location.reload();
    });
    
    
    //respond to handling event on load artist - click event
    $('body').on('click', 'a[id^="artist-"], div[id^="artist-"]', function(e) {
        
        var decodeId = $(this).attr('id').split('-');
        var id = decodeId[1];
        var name = decodeId[2];
        
        console.log('id is = ' + id + ' name is ' + name);
        
        ArtistProfile.loadArtistProfile(id, name);
    });
    
    
    //respond to handling event on load album tracks - click event
    $('body').on('click', 'li[id^="forArtist-album-"]', function(e) {
        
        //check to see if track listing is already loaded, return if so
        if($(this).hasClass('loaded')) {
            return;
        }

        var id = $(this).attr('id').split('-')[2];        
        console.log('album id = ' + id);
        
        //hide text indicator
        $(this).find('.info').fadeOut();
        
        ArtistProfile.loadTracksForAlbum(id);
    });


    //respond to custom play/stop buttons for HTML5 audio - click event
    $('body').on('click', '.startTrackSample', function(e) {

        var audioElem = $(this).next('audio');
        var button = $(this);
        
        //prevent multiple previews at once
        $('body').find('audio')[0].pause();
        
        if($(button).find('i').hasClass('fa-stop')) {
            $(audioElem)[0].pause(); //no stop() exists; pause then reset time
            $(audioElem)[0].currentTime = 0.0;
            $(button).find('i').removeClass('fa-stop').addClass('fa-play');
        } else {
            $(audioElem)[0].play();
            $(button).find('i').removeClass('fa-play').addClass('fa-stop');
        }
        
        //reset button icon back to play after 30 seconds
        setTimeout(function() {
            $(button).find('i').removeClass('fa-stop').addClass('fa-play');
        }, 30000);
    });
    
    //respond to clicking on about/reset UI - click event
    $('body').on('click', '#aboutApp', function(e) {
        e.preventDefault();
        
        $('#profileContainer').empty();
        $('#landing').show();
        $('#search').hide();
        $('#searchField').val('');
    });
});
