#CHANGE LOG

### [2015.03.10] - 2015.03.10
#### Fixed
* twitter boostrap tooltips


### [2015.03.09] - 2015.03.09
#### Fixed
* UI issue with about page/landing

#### Changed
* removed box-shadow from .img-rounded images
* the structure of the js event handler for search field and button
* disable album art carousel due to its size and clunky behavior
* the layout of artist search results due to styling issues on Safari


### [2015.03.08] - 2015.03.08
#### Added
* js code for sending request to Echo Nest for artist biography information
* the use of Echo Nest to fetch urls data for an artist
* landing page content
* genres and similar information for an artist profile if it exists

#### Changed
* js deferred objects for loading in an artist profile setcion
* tracks search results modified

#### Fixed
* issue with PHP scripts not receiving 100% JSON data from JS 
* various UI/styling issues


### [2015.03.07] - 2015.03.07
#### Added
* artist profle container page
* artist profile info
* loading in of artist's albums
* loading in of artist's tracks to albums
* loading in of artist's top 10 tracks
* custom buttons for HTML 5 audio play/stop
* js code for handling above functionality


#### Changed
* UI adjustments/styling


### [2015.03.06] - 2015.03.06
#### Added
* search for finding artists, albums, and tracks by query string
* UI scripts for structure and layout of seach results
* HTML5 audio preview for tracks

#### Changed
* results for multiple-id search against albums and tracks now filters out the data that is only necessary
* sligth UI adjustments

#### Removed
* old/unused scripts

