# MusicDiscover App

## Purpose
* The purpose of this project is to create a SPA (single page application) that does not rely on newer web frameworks such as Angular.js, Ember, or Backbone.  This project will be a starting point/foundation for creating a full MEAN stack application in the future.


## Live Demo
* The live demo can be found at [https://immense-reef-1795.herokuapp.com](https://immense-reef-1795.herokuapp.com) and runs on the Heroku free-tier option. Go to [https://www.heroku.com](https://www.heroku.com) for more information.


## This Project Uses
* Twitter Bootstrap 3 for a responsive grid and easy styling
* Font Awesome for scaleable icons
* jQuery 2.0
* PHP 5.6 with Silex Micro Framework
* Spotify REST API - for artists, albums, track
* Echo Nest REST API - for biographies, other meta data
* Deployed and running on Heroku


